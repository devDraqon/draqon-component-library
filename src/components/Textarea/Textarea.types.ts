export interface TextareaProps {
    classList?: Array<String>,
    value: string,
    onChangeCallback: Function,
    placeholderValue: string,
    columns: number,
    rows: number
}

import React, {useState} from "react";
import Textarea from './Textarea';

export default {
  title: "Textarea"
};

export const Default = () => {
    const [value, setValue] = useState("");
    const onTextChange = (text: any) => setValue(text);
    return <Textarea onChangeCallback={onTextChange} rows={8} columns={80} placeholderValue="Write something" value={value}/>;
}

import React, {useEffect} from "react";
import { TextareaProps } from "./Textarea.types";
import { GetClassName } from "draqon-modules";
import "./Textarea.scss";

const Textarea = ({classList, onChangeCallback, placeholderValue, value, columns, rows}: TextareaProps) => {
    
    const onTextChange = (e: any) => onChangeCallback(e.target.value);

    return(
        <textarea rows={rows} cols={columns} value={value} onChange={onTextChange} placeholder={placeholderValue} className={GetClassName("textarea", classList)} /> 
    )
}

export default Textarea;
 

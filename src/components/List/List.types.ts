export interface ListProps {
    classList?: Array<String>,
    listItems?: Array<any>,
    isOrdered: boolean
}
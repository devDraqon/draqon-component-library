import React, {useState} from "react";
import List from './List';

export default {
  title: "List"
};

export const OrderedList = () => {
    const [value, setValue] = useState("");
    const onNumberChange = (text: any) => setValue(text);
    return <List isOrdered={false} listItems={["item 1", "item 2", "item 3", "item 4"]}/>;
}

export const UnorderedList = () => {
    const [value, setValue] = useState("");
    const onNumberChange = (text: any) => setValue(text);
    return <List isOrdered={true} listItems={["item 1", "item 2", "item 3", "item 4"]}/>;
}

import React from "react";
import { ListProps } from "./List.types";
import { GetClassName } from "draqon-modules";
import "./List.scss";

const List = ({classList, isOrdered, listItems}: ListProps) => {
    const renderListItems = () => listItems.map((item: any, id: number) => <li className="list__item" key={id}> {item} </li> )
    return(
        (isOrdered)
            ? <ol className={GetClassName("list list__ordered", classList)}> {renderListItems()} </ol>
            : <ul className={GetClassName("list list__unordered", classList)}> {renderListItems()} </ul>
    )
}

export default List;
 

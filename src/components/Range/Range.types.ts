export interface RangeProps {
    classList?: any;
    callbackOnChange: any;
    value: number;
    min: number;
    pins: Array<string>;
    max: number;
    step: number;
}
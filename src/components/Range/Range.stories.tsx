import React, {useState} from "react";
import Range from './Range';

export default {
  title: "Range"
};

export const VolumeRange = () => {
    const [value, setValue] = useState(1);
    const onTextChange = (text: any) => setValue(text);
    return <Range pins={["0%", "25%", "50%", "75%", "100%"]} min={1} max={100} step={0.01} callbackOnChange={onTextChange} value={value}/>;
}
export const SpeedRange = () => {
    const [value, setValue] = useState(1);
    const onTextChange = (text: any) => setValue(text);
    return <Range pins={["0.5x", "0.75x", "1.0x", "1.25x", "1.5x"]} min={1} max={100} step={0.01} callbackOnChange={onTextChange} value={value}/>;
}

export const RangeWithWrapper = () => {
  const [value, setValue] = useState(1);
  const onTextChange = (text: any) => setValue(text);
  return <div className="wrapper">
    <Range pins={["0%", "25%", "50%", "75%", "100%"]} min={1} max={100} step={0.01} callbackOnChange={onTextChange} value={value} />
  </div>
}
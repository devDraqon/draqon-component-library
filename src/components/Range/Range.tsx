import React from "react";
import { RangeProps } from "./Range.types";
import { GetClassName } from "draqon-modules";
import "./Range.scss";

const Range = ({classList, step, min, max, callbackOnChange, value, pins}: RangeProps) => {
    const onChange = (e: any) => callbackOnChange(e.target.value);
    return(
        <div className="range__container">
            <div className="range__pins"> 
                {pins.map((pin, pinID) => <span key={pinID} className={"range__pin-"+pinID}> {pin} </span>)}
            </div>
            <input  min={min} max={max} step={step} type="range" className={GetClassName("range", classList)} value={value} onChange={onChange} />
        </div>
    )
}

export default Range;
 

import React from "react";
import { HeadlineProps } from "./Headline.types";
import { GetClassName } from "draqon-modules";
import "./Headline.scss";

const Headline = ({children, classList, size}: HeadlineProps) => {
    return(
        size.toString() === "teaserxl" ? <h1 className={GetClassName("headline headline__TeaserXL", classList)}> {children} </h1> :
        size.toString() === "teaser" ? <h1 className={GetClassName("headline headline__Teaser", classList)}> {children} </h1> :
        size.toString() === "xxlarge" ? <h1 className={GetClassName("headline headline__XXLarge", classList)}> {children} </h1> :
        size.toString() === "xlarge" ? <h2 className={GetClassName("headline headline__XLarge", classList)}> {children} </h2> :
        size.toString() === "large" ? <h3 className={GetClassName("headline headline__Large", classList)}> {children} </h3> :
        size.toString() === "medium" ? <h4 className={GetClassName("headline headline__Medium", classList)}> {children} </h4> :
        size.toString() === "small" ? <h5 className={GetClassName("headline headline__Small", classList)}> {children} </h5> :
        size.toString() === "xsmall" ? <h6 className={GetClassName("headline headline__XSmall", classList)}> {children} </h6> : null
    )
}

export default Headline;
 

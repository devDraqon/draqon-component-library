import React from "react";
import Headline from './Headline';

export default {
  title: "Headline"
};

export const TeaserXL = () => <Headline children="Extra Large Teaser Headline" size="teaserxl" />;
export const Teaser = () => <Headline children="Teaser Headline" size="teaser" />;
export const XXLarge = () => <Headline children="Extra Extra Large Headline" size="xxlarge" />;
export const XLarge = () => <Headline children="Extra Large Headline" size="xlarge" />
export const Large = () => <Headline children="Large Headline" size="large" />
export const Medium = () => <Headline children="Medium Headline" size="medium" />
export const Small = () => <Headline children="Small Headline" size="small" />
export const XSmall = () => <Headline children="Extra Small Headline" size="xsmall" />
export interface HeadlineProps {
  children: any,
  classList?: Array<String>,
  size: "teaserxl" | "teaser" | "xxlarge" | "xlarge" | "large" | "medium" | "small" | "xsmall"
} 
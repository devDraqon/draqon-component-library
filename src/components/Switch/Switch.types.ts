export interface SwitchProps {
    callbackOnClick: Function | void;
    isActive?: boolean;
}
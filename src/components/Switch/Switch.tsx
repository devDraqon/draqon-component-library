import React from "react";
import { SwitchProps } from "./Switch.types";
import { GetClassName } from "draqon-modules";
import "./Switch.scss";

/* Each input element can be controlled or uncontrolled
   All of my input elements are uncontrolled, their parent component will control them. */
const Switch = ({callbackOnClick, isActive}: SwitchProps) => {
    
    const localOnClick = () => {
        console.log("CLICK ON SLIDER");
        callbackOnClick ? callbackOnClick() : null;
    } 
    
    return (
        <label onClick={localOnClick} className={isActive ? "switch switch__active" : "switch switch__inactive"}>
        <div className={isActive ? "slider slider__active" : "slider slider__inactive"}></div>
        </label>
    )
}

export default Switch;
import React, {useState} from "react";
import Switch from './Switch';

export default {
  title: "Switch"
};

export const Default = () => {
    const [isActive, setIsActive] = useState(false);
    return <Switch isActive={isActive} callbackOnClick={() => setIsActive(!isActive)}/>;
}

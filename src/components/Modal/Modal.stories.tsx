import React from "react";
import Modal from './Modal';
import Headline from "../Headline/Headline";
import Paragraph from "../Paragraph/Paragraph";

export default {
  title: "Modal"
};

const ParagraphContent = () => "Duis laoreet luctus orci, id elementum augue congue non. Fusce ac efficitur tellus, sit amet hendrerit augue. Maecenas scelerisque, mauris eu iaculis porttitor, est mauris fringilla purus, id tempus diam arcu et tortor. Curabitur ut mauris bibendum urna interdum molestie. Phasellus fermentum eget tortor quis feugiat. Pellentesque venenatis id diam a fringilla. Nulla in convallis neque, id fringilla mauris."

export const Top = () => <Modal position="top"> 
  <Headline size ="large"> Modal Headline </Headline>
  <Paragraph size ="medium"> {ParagraphContent()} </Paragraph>
</Modal>

export const Left = () => <Modal position="left"> 
  <Headline size ="large"> Modal Headline </Headline>
  <Paragraph size ="medium"> {ParagraphContent()} </Paragraph>
</Modal>

export const LeftPremium = () => <Modal optionalLogoSrc="https://files.draqondevelops.com/images/logodragon.png" position="left"> 
  <Headline size ="large"> Modal Headline </Headline>
  <Paragraph size ="medium"> {ParagraphContent()} </Paragraph>
</Modal>

export const Right = () => <Modal position="right"> 
  <Headline size ="large"> Modal Headline </Headline>
  <Paragraph size ="medium"> {ParagraphContent()} </Paragraph>
</Modal>

export const Bottom = () => <Modal position="bottom"> 
  <Headline size ="large"> Modal Headline </Headline>
  <Paragraph size ="medium"> {ParagraphContent()} </Paragraph>
</Modal>

export const Center = () => <Modal position="center"> 
  <Headline size ="large"> Modal Headline </Headline>
  <Paragraph size ="medium"> {ParagraphContent()} </Paragraph>
</Modal>

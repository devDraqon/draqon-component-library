import React, {useState, useEffect} from "react";
import {GetClassName} from "draqon-modules";
import {ModalProps} from "./Modal.types";
import Icon from "../Icon/Icon";
import Image from "../Image/Image";
import "./Modal.scss";

const Modal = ({callbackOnClose, children, classList, position, optionalLogoSrc, blurNodeArr}: ModalProps) => {
    
    const [isOpen, toggleIsOpen] = useState(true);

    const onClickCloseIcon = () => {
        if(blurNodeArr)
            if(blurNodeArr.length > 0)
                for(let i=0; i<blurNodeArr.length; i++) {
                    blurNodeArr[i].classList.remove("noscroll");
                    blurNodeArr[i].classList.remove("blur");
                }
        toggleIsOpen(!isOpen);
        callbackOnClose ? callbackOnClose() : null;
    }
    
    useEffect(() => {
        if(blurNodeArr)
            if(blurNodeArr.length > 0)
                for(let i=0; i<blurNodeArr.length; i++) {
                    blurNodeArr[i].classList.add("noscroll");
                    blurNodeArr[i].classList.add("blur");
                }
    })

    return(
        (isOpen)
            ? <div className={GetClassName("modal modal__"+position, classList)}> 
                {optionalLogoSrc ? <Image src={optionalLogoSrc} alt="logodragon" /> : null}
                <Icon callbackOnClick={onClickCloseIcon} alt="close Icon" classList={["modal__icon"]} src="https://files.draqondevelops.com/images/close_dark.png" />
                <div className={"modal__content"}> {children} </div>
            </div> 
            : null
    )
}

export default Modal;
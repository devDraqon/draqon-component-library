export interface ModalProps {
  classList?: any;
  children: any;
  position: "top" | "left" | "bottom" | "right" | "center";
  callbackOnClose?: any;
  blurNodeArr?: Array<any>;
  optionalLogoSrc?: string;
}
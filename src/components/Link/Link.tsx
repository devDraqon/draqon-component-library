import React from "react";
import {GetClassName} from "draqon-modules";
import {Link as ReactLink} from "react-router-dom";
import {LinkProps} from "./Link.types";
import "./Link.scss";

const Link = ({isReactLink, target, url, classList, children}: LinkProps) => {

    return isReactLink 
        ? <ReactLink to={url} target={target} className={GetClassName("link link__internal", classList)}>{children}</ReactLink>
        : <a target={target} className={GetClassName("link link__external", classList)} href={url}>{children}</a>
}

export default Link;
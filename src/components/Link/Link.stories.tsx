import React from "react";
import {Route, BrowserRouter, Switch, Redirect} from "react-router-dom";
import Paragraph from "../Paragraph/Paragraph";
import Link from "./Link";

export default {
    title: "Link"
}

export const Internal = () => <BrowserRouter>
    <Redirect to="/" />
    <Switch>
        <Route exact path="/"> <Paragraph size="medium"><Link target="_self" isReactLink={true} url="page2"> visit page 2 </Link> before you think of doing anything else. </Paragraph> </Route>
        <Route path="/page2"> <Link target="_self" isReactLink={true} url="/"> back to homepage </Link> </Route>
        <Route> Error 404, Route not found </Route>
    </Switch>
</BrowserRouter>

export const External = () => <Paragraph size="medium"> <Link target="_blank" isReactLink={false} url="https://google.com"> Click Me</Link> to visit google.com </Paragraph>
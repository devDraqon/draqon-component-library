import React from "react";

export interface LinkProps {
    isReactLink: boolean,
    url: string,
    classList?: Array<string>,
    children: React.ReactNode | any,
    target: "_blank" | "_self" | "_parent" | "_top"
}
import React, {useState} from "react";
import Textbox from './Textbox';

export default {
  title: "Textbox"
};

export const Default = () => {
    const [value, setValue] = useState("");
    const onTextChange = (text: any) => setValue(text);
    return <Textbox onChangeCallback={onTextChange} placeholderValue="Write something" value={value}/>;
}

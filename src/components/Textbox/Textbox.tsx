import React from "react";
import { TextboxProps } from "./Textbox.types";
import { GetClassName } from "draqon-modules";
import "./Textbox.scss";



const Textbox = ({classList, onChangeCallback, placeholderValue, value}: TextboxProps) => {
    const onChange = (e: any) => onChangeCallback(e.target.value);
    return(
        <input type="textbox" className={GetClassName("textbox", classList)} value={value} placeholder={placeholderValue} onChange={onChange} />
    )
}

export default Textbox;
 

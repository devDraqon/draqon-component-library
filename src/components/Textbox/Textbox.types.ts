export interface TextboxProps {
    classList?: Array<String>,
    value: string,
    onChangeCallback: Function,
    placeholderValue: string
}
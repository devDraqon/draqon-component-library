import React from "react";
import Spacer from './Spacer';

export default {
  title: "Spacer"
};

export const Default = () => <span> 
  <Spacer children="" /> 
  <p> This content appears after the spacer, so you need to scroll down to see it.  </p>
</span>
import React from "react";
import {SpacerProps} from "./Spacer.types";
import "./Spacer.scss"; 

const Spacer = ({children}: SpacerProps) => {
    return(
        <div className="spacer">
            {children}
        </div>
    )
}

export default Spacer;
 

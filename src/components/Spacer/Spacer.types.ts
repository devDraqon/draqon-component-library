export interface SpacerProps {
  children: React.ReactNode | String,
}
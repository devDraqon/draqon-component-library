import React, { useState } from "react";
import { DropdownProps } from "./Dropdown.types";
import { GetClassName } from "draqon-modules";
import Icon from "../Icon/Icon";
import Flex from "../Flex/Flex";
import "./Dropdown.scss";



const Dropdown = ({ classList, placeholder, labels, callbackOnChange }: DropdownProps) => {

    const [isOpen, setIsOpen] = useState(false);
    
    const [dropdownState, setDropdownState] = useState({
        selectedID: placeholder ? -1 : 0, items: labels.map((label) => {
            return { name: label, checked: false };
        })
    })

    const onSelectionChange = (selectedDropdownButtonID: any) => {
        let activeID = -1;
        const items = dropdownState.items.map((item: any, itemID: number) => {
            item.checked = (selectedDropdownButtonID === itemID) ? !item.checked : false;
            if (selectedDropdownButtonID === itemID) activeID = (item.checked) ? itemID : -1;
            return item;
        });
        setDropdownState({ selectedID: activeID, items: items })
        toggleDropDown();
        callbackOnChange ? callbackOnChange(activeID) : null;
    };

    const toggleDropDown = () => {
        setIsOpen(!isOpen);
    }
    
    return (
        <div className={GetClassName("dropdown", classList)}>
            <span onClick={toggleDropDown}>
                <Flex direction="horizontal">
                    <div className="dropdown__head-item" > {(dropdownState.selectedID >= 0) ? dropdownState.items[dropdownState.selectedID].name : placeholder } </div>
                    <Icon classList={isOpen ? ["rotated"] : undefined} callbackOnClick={null} alt="icon" src="https://files.draqondevelops.com/images/arrow.png" />
                </Flex>
            </span>

            {(isOpen) 
                ? dropdownState.items.map((item: any, itemID: number) =>
                    (!item.checked) 
                        ? <div className="dropdown__body-item" onClick={() => onSelectionChange(itemID)}> {item.name} </div> 
                        : null ) 
                : null }
            {(isOpen && dropdownState.selectedID >= 0 && placeholder) ? <div className="dropdown__body-item" onClick={() => onSelectionChange(-1)}> {placeholder} </div> : null }
        </div>
    )
}

export default Dropdown;


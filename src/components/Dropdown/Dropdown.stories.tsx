import React from "react";
import Dropdown from './Dropdown';

export default {
  title: "Dropdown"
};

export const WithPlaceholder = () => {
    const callbackOnChange = (selectedID) => console.log("active dropdown element: "+selectedID)
    return <Dropdown placeholder="Placeholder Element" labels={["Dropdown Element 1", "Dropdown Element 2", "Dropdown Element 3", "Dropdown Element 4", "Dropdown Element 5"]} callbackOnChange={callbackOnChange} />;
}

export const WithoutPlaceholder = () => {
    const callbackOnChange = (selectedID) => console.log("active dropdown element: "+selectedID)
    return <Dropdown labels={["Dropdown Element 1", "Dropdown Element 2", "Dropdown Element 3", "Dropdown Element 4", "Dropdown Element 5"]} callbackOnChange={callbackOnChange} />;
}

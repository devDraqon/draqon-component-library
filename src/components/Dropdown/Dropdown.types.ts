export interface DropdownProps {
    classList?: any;
    callbackOnChange: any;
    placeholder?: string;
    labels: Array<string>;
}
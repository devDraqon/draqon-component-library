export interface LabelProps {
  classList?: Array<String>,
  text: string,
}

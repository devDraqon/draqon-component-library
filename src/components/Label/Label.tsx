import React from "react";
import {GetClassName} from "draqon-modules";
import Paragraph from "../Paragraph/Paragraph";
import {LabelProps} from "./Label.types";
import "./Label.scss";

const Label = ({text, classList}: LabelProps) => {
    return(
        <div className={GetClassName("label", classList)}>
            <div className="label__peak-left" />
            <Paragraph classList={["label__text"]} size="medium" children={text} />
            <div className="label__peak-right" />
        </div>
    )
}

export default Label;
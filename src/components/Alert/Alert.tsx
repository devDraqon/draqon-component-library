import React, {useState} from "react";
import {AlertProps} from "./Alert.types";
import "./Alert.scss";
import Icon from "../Icon/Icon";
import Headline from "../Headline/Headline";
import Paragraph from "../Paragraph/Paragraph";
import { GetClassName } from "draqon-modules";

const Alert = ({classList, description, title, type}: AlertProps) => {
    const [isOpen, setIsOpen] = useState(true);
    const callbackOnClick = () => setIsOpen(!isOpen)
    return (
        isOpen ? 
        <div className={GetClassName("alert alert__"+type, classList)}> 
            <Icon callbackOnClick={callbackOnClick} alt="close Icon" classList={["alert__icon"]} src="https://files.draqondevelops.com/images/close_dark.png" />
            <div className="alert__message"> 
                {title ? <Headline size="small"> {title} </Headline> : null}
                <Paragraph size="small"> {description} </Paragraph> 
            </div>
        </div> : null
    )
}

export default Alert
import React from "react";
import Alert from './Alert';

export default {
  title: "Alert"
};

export const InfoSmall = () => <Alert type="info" description="Client Info Message" />
export const InfoBig = () => <Alert type="info" title="Client Info" description="Client Info Message" />
export const SuccessSmall = () => <Alert type="success" description="Client Success Message" />
export const SuccessBig = () => <Alert type="success" title="Client Success" description="Client Success Message" />
export const WarningSmall = () => <Alert type="warning" description="Client Warning Message" />
export const WarningBig = () => <Alert type="warning" title="Client Warning" description="Client Warning Message" />
export const ErrorSmall = () => <Alert type="error" description="Client Error Message" />
export const ErrorBig = () => <Alert type="error" title="Client Error" description="Client Error Message" />


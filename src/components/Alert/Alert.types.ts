export interface AlertProps {
    classList?: Array<string>;
    title?: string;
    description: string;
    type: "info" | "error" | "warning" | "success";
}
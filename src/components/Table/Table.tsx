import React, { useState, useEffect } from "react";
import { TableProps } from "./Table.types";
import "./Table.scss";
import { GetClassName } from "draqon-modules";

const Table = ({ classList, tableContent }: TableProps) => {

    const addSortedPropertyToColumns = () => {
        let newArray = [];
        tableContent.columns.map((column, columnID) => {newArray.push({
            name: column,
            sorted: 0
        })})
        return newArray;
    }

    const getUnsortedIDs = () => {
        let newArray = [];
        tableContent.rows.map((row, rowID) => newArray.push(rowID))
        return newArray;
    }

    const [columns, setColumns]: any = useState(() => addSortedPropertyToColumns())
    const [sortedIDs, setSortedIDs]: any = useState(() => getUnsortedIDs())


    const getValuesFromSpecificColumnInAllRows = (columnID) => {
        let values = [];
        tableContent.rows.map((row, rowID) =>
            row.map((entry, entryID) => {
                if (entryID === columnID) values.push(entry);
            })
        )
        return values;
    }

    const prepareSort = (columnID, isReversed) => {
        let values = getValuesFromSpecificColumnInAllRows(columnID)
        setSortedIDs(isNaN(values[0]) ? sortArray(values, isReversed, false) : sortArray(values, isReversed, true))
        return;
    }

    const sortArray = (unsortedArray, sortReversed, sortNumeric) => {
        let sortedOrder = [];
        let sortedArray = (sortNumeric) ? JSON.parse(JSON.stringify(unsortedArray)).sort(function(a, b){return a-b}) : JSON.parse(JSON.stringify(unsortedArray)).sort();
        sortedArray.map((word, wordID) => sortedOrder.push(unsortedArray.indexOf(word)));
        if (sortReversed) sortedOrder = sortedOrder.reverse();
        return sortedOrder;
    }

    const updateSortDirection = (columnID) => {
        setColumns(columns.map((column, colID) => {
            if (columnID === colID) {
                if (column.sorted === 0) column.sorted = 1;
                else if (column.sorted === 1) column.sorted = -1;
                else column.sorted = 0;
            } else column.sorted = 0;
            return column;
        }))
    }

    

    const onClick = (columnID, columnSortState) => {
        updateSortDirection(columnID);
        if (columns[columnID].sorted === 0) setSortedIDs(getUnsortedIDs());
        else if (columns[columnID].sorted === 1) prepareSort(columnID, false)
        else if (columns[columnID].sorted === -1) prepareSort(columnID, true)
    }

    const returnRows = () => 
        sortedIDs.map((sortedID: any, rowID) =>
            <tr key={rowID}>
                {returnEntry(sortedID)}
            </tr>)
    
    const returnEntry = (sortedID) => 
        tableContent.rows[sortedID].map((entryName, entryID) => 
            <td key={entryID}> 
                {entryName} 
            </td>)

    const returnColumns = () => 
        columns.map((column, columnID) => 
            <th onClick={() => onClick(columnID, column.sorted)} key={columnID}> 
                {column.name} 
            </th>)

    return (
        <table className={GetClassName("table", classList)}>
            <thead>
                <tr>
                    {returnColumns()}
                </tr>
            </thead>
            <tbody>
                {returnRows()}
            </tbody>
        </table>
    )
}

export default Table
export interface TableProps {
    classList?: Array<string>;
    tableContent: {
        columns: Array<string>,
        rows: Array<Array<number|string|boolean>>;
    }
} 
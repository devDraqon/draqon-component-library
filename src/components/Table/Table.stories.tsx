import React from "react";
import Table from './Table';

export default {
  title: "Table"
};
const somejson = {
  columns: [
      "Age",
      "Name",
      "Code",
  ],
  rows: [
      [
          42,
          "John",
          "Doe"
      ],
      [
          44,
          "Mighty",
          "Evil"
      ],
      [
          7,
          "Tesla",
          "Mon"
      ],
      [
          99,
          "Max",
          "Number"
      ],
      [
          13,
          "Thirteen",
          "Monster"
      ],
      [
          66,
          "Investigator",
          "Seven"
      ],
  ]
}
export const Default = () => <Table tableContent={somejson}/>

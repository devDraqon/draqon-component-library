import React from "react";
import {GetClassName} from "draqon-modules";
import {FlexProps} from "./Flex.types";
import "./Flex.scss";

const Flex = ({classList, direction, children}: FlexProps) => {

    return (
        <div className={GetClassName("flex flex__"+direction, classList)}>
            {children}
        </div>
    )
}

export default Flex;
import React from "react";
import Flex from "./Flex";
import Image from "../Image/Image";

export default {
    title: "Flex"
}

export const Horizontal = () => 
    <Flex direction="horizontal">
        <Image alt="Image" src="https://files.draqondevelops.com/images/lava_dragon.jpg" />
        <Image alt="Image" src="https://files.draqondevelops.com/images/lava_dragon.jpg" />
        <Image alt="Image" src="https://files.draqondevelops.com/images/lava_dragon.jpg" />
    </ Flex>
export const Vertical = () => 
    <Flex direction="vertical">
        <Image alt="Image" src="https://files.draqondevelops.com/images/lava_dragon.jpg" />
        <Image alt="Image" src="https://files.draqondevelops.com/images/lava_dragon.jpg" />
        <Image alt="Image" src="https://files.draqondevelops.com/images/lava_dragon.jpg" />
    </ Flex>
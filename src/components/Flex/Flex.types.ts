export interface FlexProps {
    classList?: Array<string>;
    direction: "horizontal" | "vertical";
    children: any;
}
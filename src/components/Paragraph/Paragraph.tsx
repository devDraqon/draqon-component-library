import React from "react";
import {GetClassName} from "draqon-modules";
import {ParagraphProps} from "./Paragraph.types";
import "./Paragraph.scss";

const Paragraph = ({children, classList, size}: ParagraphProps) => {
    return(
        <p className={GetClassName("paragraph "+size, classList)}> 
            {children}
        </p>
    )
}

export default Paragraph;
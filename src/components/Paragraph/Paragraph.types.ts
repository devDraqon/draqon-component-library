export interface ParagraphProps {
  children: String | React.ReactNode,
  size: "medium" | "large" | "small";
  classList?: any;
}
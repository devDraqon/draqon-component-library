import React from "react";
import Paragraph from './Paragraph';

export default {
  title: "Paragraph"
};

export const Large = () => <Paragraph size="large"> Large Paragraph </Paragraph>
export const Medium = () => <Paragraph size="medium"> Medium Paragraph  </Paragraph>
export const Small = () => <Paragraph size="small"> Small Paragraph </Paragraph>

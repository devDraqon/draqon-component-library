import React from "react";
import Image from './Image';

export default {
  title: "Image"
};

export const Responsive = () => <Image alt="Image" src="https://files.draqondevelops.com/images/dragon.jpg" />
export const ResponsiveBig = () => <Image alt="Image" src="https://files.draqondevelops.com/images/rocket.jpeg" />

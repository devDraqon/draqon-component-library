export interface ImageProps {
  src: string;
  classList?: Array<String>,
  alt?: string;
  test?: boolean;
}
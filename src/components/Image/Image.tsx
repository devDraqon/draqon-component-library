import React from "react";
import {GetClassName} from "draqon-modules";
import {ImageProps} from "./Image.types";
import "./Image.scss";

const Image = ({src, alt, classList, test}: ImageProps) => {
    return(
        <img src={src} className={GetClassName("image", classList)} alt={alt? alt : "image"} />
    )
}





export default Image;
 

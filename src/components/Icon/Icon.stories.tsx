import React from "react";
import Icon from './Icon';

export default {
  title: "Icon"
};

export const Default = () => <Icon callbackOnClick={null} alt="Download Icon" src="https://files.draqondevelops.com/icons/download.svg" />

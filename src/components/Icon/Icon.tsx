import React, {useState, useEffect} from "react";
import {GetClassName} from "draqon-modules";
import {IconProps} from "./Icon.types";
import "./Icon.scss";

const Icon = ({src, callbackOnClick, classList, alt}: IconProps) => {

    const localOnClick = () => {
        callbackOnClick ? callbackOnClick() : null;
    }

    return (
        <img onClick={localOnClick} className={GetClassName("icon", classList)} src={src} alt={alt} />
    )
}

export default Icon;
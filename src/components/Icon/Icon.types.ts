export interface IconProps {
  classList?: any,
  callbackOnClick: any,
  src: string,
  alt: string
}
import React from "react";
import { NumberboxProps } from "./Numberbox.types";
import { GetClassName, GetNumberInput } from "draqon-modules";
import "./Numberbox.scss";

const Numberbox = ({classList, min, max, onChangeCallback, placeholderValue, value}: NumberboxProps) => {
    const onChange = (e: any) => 
(e.target.value !== "") ? onChangeCallback(GetNumberInput(e.target.value, min, max)) : onChangeCallback("");        
    
    return (
        <input type="number" min={min} max={max} className={GetClassName("numberbox", classList)} value={value} placeholder={min + " - " + max} onChange={onChange} />
    )
}

export default Numberbox;
 

import React, {useState} from "react";
import Numberbox from './Numberbox';

export default {
  title: "Numberbox"
};

export const Default = () => {
    const [value, setValue] = useState("");
    const onNumberChange = (text: any) => setValue(text);
    return <Numberbox min={1} max={420} onChangeCallback={onNumberChange} placeholderValue="1234567890" value={value}/>;
}

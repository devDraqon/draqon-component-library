export interface NumberboxProps {
    classList?: Array<String>,
    value: string,
    min: number,
    max: number,
    onChangeCallback: Function,
    placeholderValue: string
}
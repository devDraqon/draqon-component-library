import React, {useState, useEffect} from "react";
import {GetClassName} from "draqon-modules";
import {CodeProps} from "./Code.types";
import Headline from "../Headline/Headline";
import "./Code.scss";

const Code = ({title, isInline, children, classList}: CodeProps) => {

    return (
        (isInline)
            ? <code className={GetClassName("code code__inline", classList)}>{children}</code>
            : <div className={GetClassName("code code__block", classList)}>
                {(title) ? <Headline classList={["code__headline"]} size="xsmall" >{title}</Headline> : null}
                <code className={GetClassName("code__block-text", classList)}>{children}</code>
              </div>
    )
}

export default Code;
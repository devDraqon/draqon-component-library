export interface CodeProps {
  children: any,
  isInline: boolean,
  classList?: any,
  title?: string
}
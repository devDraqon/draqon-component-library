import React from "react";
import Code from './Code';
import Paragraph from "../Paragraph/Paragraph";

export default {
  title: "Code"
};

export const Inline = () => <Paragraph size="small"> This text represents a span holding normal text and <Code isInline={true}> this inline code() </Code> together. This can be useful to clarify variables or to highlight functions and to preview this whole shit as it goes with even more text which doesnt even make sense to read like complete nonesense but its sure okay to check the line-height and margin to fit to the inline code</Paragraph>
export const Multiline = () => <Code isInline={false}>
{`import React, {useState, useEffect} from "react";
 import {GetClassName} from "draqon-modules";
 import {CodeProps} from "./Code.types";
 import Headline from "../Headline/Headline";
 import "./Code.scss";
 
 const Code = ({title, isInline, children, classList}: CodeProps) => {
 
     return (
         (isInline)
             ? <code className={GetClassName("code code__inline", classList)}> {children} </code>
             : <div className={GetClassName("code code__block", classList)}>
                 {(title) ? <Headline classList={["code__headline"]} size="small" > {title}  </Headline> : null}
                 <code className={GetClassName("code__block-text", classList)}> {children} </code>
               </div>
     )
 }

 export default Code;`} </Code>
export const MultilineWithTitle = () => <Code title="Code.tsx" isInline={false}>
{`import React, {useState, useEffect} from "react";
 import {GetClassName} from "draqon-modules";
 import {CodeProps} from "./Code.types";
 import Headline from "../Headline/Headline";
 import "./Code.scss";
 
 const Code = ({title, isInline, children, classList}: CodeProps) => {
 
     return (
         (isInline)
             ? <code className={GetClassName("code code__inline", classList)}> {children} </code>
             : <div className={GetClassName("code code__block", classList)}>
                 {(title) ? <Headline classList={["code__headline"]} size="small" > {title}  </Headline> : null}
                 <code className={GetClassName("code__block-text", classList)}> {children} </code>
               </div>
     )
 }
 
 export default Code;`}
</Code>

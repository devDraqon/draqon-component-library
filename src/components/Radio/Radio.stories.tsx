import React from "react";
import Radio from './Radio';

export default {
  title: "Radio"
};

export const Default = () => {
    const callbackOnChange = (selectedID) => console.log("active radio element: "+selectedID)
    return <Radio name="group1"  labels={["Radio Element 1", "Radio Element 2", "Radio Element 3", "Radio Element 4", "Radio Element 5"]} callbackOnChange={callbackOnChange} />;
}

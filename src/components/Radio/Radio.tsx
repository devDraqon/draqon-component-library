import React, { useState } from "react";
import { RadioProps } from "./Radio.types";
import { GetClassName } from "draqon-modules";
import Headline from "../Headline/Headline";
import Flex from "../Flex/Flex";
import "./Radio.scss";

const Radio = ({ classList, name, labels, callbackOnChange }: RadioProps) => {

    const [radioState, setRadioState] = useState({
        selectedID: -1, items: labels.map((label) => {
            return { name: label, checked: false };
        })
    })
    const onSelectionChange = (selectedRadioButtonID: any) => {
        let activeID = -1;
        const items = radioState.items.map((item: any, itemID: number) => {
            item.checked = (selectedRadioButtonID === itemID) ? !item.checked : false;
            if (selectedRadioButtonID === itemID) activeID = (item.checked) ? itemID : -1;
            return item;
        });
        setRadioState({ selectedID: activeID, items: items })
        callbackOnChange ? callbackOnChange(activeID) : null;
    };

    return (
        <div className={GetClassName("radio", classList)}>
            {radioState.items.map((item: any, itemID: number) =>
                <Flex direction="horizontal" key={itemID}>
                    <input className="radio__item-input" readOnly checked={item.checked} type="radio" name={name} onClick={() => onSelectionChange(itemID)} />
                    <Headline classList={[item.checked ? "radio__headline radio__headline-checked" : "radio__headline"]} size="xsmall"> {item.name} </Headline>
                </Flex>
            )}
        </div>
    )
}

export default Radio;


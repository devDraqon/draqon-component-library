export interface RadioProps {
    classList?: any;
    callbackOnChange: any;
    labels: Array<string>;
    name: string;
}
import React, {useState, useEffect} from "react";
import {GetClassName} from "draqon-modules";
import {ButtonProps} from "./Button.types";
import "./Button.scss";

const Button = ({callbackOnClick, value, classList}: ButtonProps) => {

    const localOnClick = () => {
        callbackOnClick ? callbackOnClick() : null;
    }

    return (
        <input value={value} onClick={localOnClick} type="button" className={GetClassName("button", classList)} />
    )
}

export default Button;
import peerDepsExternal from "rollup-plugin-peer-deps-external";
import resolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";
import typescript from "rollup-plugin-typescript2";
import postcss from "rollup-plugin-postcss";
import font from "rollup-plugin-font";

const packageJson = require("./package.json");

export default {
  input: "src/index.ts", // Rollup will build up a dependency graph from this entry point and then bundle all the components that are imported/exported
  output: [ // is an array with two objects, each specifying output config (CJS and ESM)
    {
      file: packageJson.main,
      format: "cjs",
      sourcemap: true
    },
    {
      file: packageJson.module,
      format: "esm",
      sourcemap: true
    }
  ],
  plugins: [
    peerDepsExternal(),
    resolve(),
    commonjs(),
    typescript({ useTsconfigDeclarationDir: true }), // it outputs the .d.ts files in the directory specified by in tsconfig.json
    postcss(),
    font({
			"svg":"./node_modules/font-awesome/fonts/fontawesome-webfont.svg",
			"include": [
				"node_modules/fontawesome/**"
			],
			"outDir":"fontawesome",
			"output":['svg', 'ttf', 'eot', 'woff', 'woff2'],
			"whiteList":["bell"]
		})
  ]
};
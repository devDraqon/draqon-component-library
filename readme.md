# Draqons Comoponent Library
React Component Library for widely used components.

## Live Demo
<a href="https://storybook.draqondevelops.com/react-component-library"> click here </a> to visit a static live version of this module.

## Installation
    npm install --save draqon-component-library

## Component Overview
* Alert
* Button
* Code
* Dropdown
* Flex
* Headline
* Icon
* Label
* Link
* List
* Modal
* Numberbox
* Paragraph
* Radio
* Range
* Spacer
* Switch
* Table
* Textarea
* Textbox

### Importing a component
    import {Headline} from 'draqon-component-library';

### Source Code
<a href="https://gitlab.com/devdraqon/draqon-component-library"> visit the source code for this project here. </a>

### Known Bugs
-   no documentated known bugs

### Report Bugs and Issues
If you encounter any bugs, issues, or have improvements which you'd like to share, please choose one of the following ways to report your issue.
- <a href="https://gitlab.com/devDraqon/draqon-component-library/-/issues"> Report Issue on Gitlab </a> (requires having a gitlab account).
- <a href="mailto:draqondevelops@gmail.com"> Mail your Issue </a> to me.

### Version History
-   Start of documentated version history 